# db sherpa
The point of this tool is to automate snapshotting our database and then restoring it to another server.

# configuration
The configuration for the application expects to have ```ENV_FILE_LOCATION``` set. This file should have the following information filled out:

```
output: text
profile: tempalertdev
db:
  mssql:
    conn: testConnstring
```

A full example can be found in [dev.yml](config/dev.yml)

# running it
There is a simple run script [build/run.sh](build/run.sh) that sets the environment to dev and then runs the app.