package main

import (
	"log"

	_ "bitbucket.org/taelliot/db-sherpa/src/config"
	
	aws "bitbucket.org/taelliot/db-sherpa/src/aws"
	dbfactory "bitbucket.org/taelliot/db-sherpa/src/sql/factory"
)

func main() {
	log.Printf("Running using profile:. %s\n", aws.Credentials.Profile)
	_, err:= dbfactory.New("mssql")
	if err != nil {
		log.Fatalf("Could not create connection to db.")
	}
}
