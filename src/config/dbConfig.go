package config

import (
	"gopkg.in/yaml.v2"
	"log"

	sql "bitbucket.org/taelliot/db-sherpa/src/sql"
)

var DBConfig dbconfig

func init() {
	log.Println("DBConfig init.")
	DBConfig.Parse(*RawConfig)
}

type dbconfig struct {
	Providers map[string]sql.Connection `yaml:"db"`
}

func (c *dbconfig) Parse(data []byte) error {
	err := yaml.Unmarshal(data, c)
	if err != nil {
		log.Fatal(err)
	}
	return nil
}
