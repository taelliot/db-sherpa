package config

import (
	"io/ioutil"
	"log"
	"os"
)

var RawConfig *[]byte

var loaded = parseConfiguration()

func parseConfiguration() bool{
	log.Println("Runtime config init.")
	environment_file := os.Getenv("ENV_FILE_LOCATION")
	if environment_file == "" {
		log.Fatalf("No ENV_FILE_LOCATION specified.")
	}
	cfg, err := ioutil.ReadFile(environment_file)
	if err != nil {
		log.Fatal(err)
	}
	RawConfig = &cfg
	log.Printf("Loaded configuration for %s.\n", environment_file)
	return true
}
