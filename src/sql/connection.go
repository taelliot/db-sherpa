package mssql

import (
	"gopkg.in/yaml.v2"
	"log"
)

type Connection struct {
	ConnectionString string `yaml:"conn"`
}

func (c *Connection) Parse(data []byte) error {
	err := yaml.Unmarshal(data, c)
	if err != nil {
		log.Fatal(err)
	}
	if c.ConnectionString == "" {
		log.Fatalf("ConnectionString not set in environment_db file.")
	}
	return nil
}
