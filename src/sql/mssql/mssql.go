package mssql

import (
	"log"
	
	s "bitbucket.org/taelliot/db-sherpa/src/sql"
	sherpa "bitbucket.org/taelliot/db-sherpa/src/sql/sherpa"
)

type db struct {
}

func New(connection s.Connection) sherpa.Sherpa {
	log.Printf("Making new mssql db connection with connString %s", connection.ConnectionString)
	return db{}
}

func (db db) DropDB(name string) error {
	log.Printf("Dropping db: %\n", name)
	return nil
}

func (db db) AttachDB(path string) error {
	log.Printf("Attaching db from path: %\n", path)
	return nil
}
