package factory

import (
	"fmt"
	"log"

	cfg "bitbucket.org/taelliot/db-sherpa/src/config"
	"bitbucket.org/taelliot/db-sherpa/src/sql/mssql"
	"bitbucket.org/taelliot/db-sherpa/src/sql/sherpa"
)

func New(provider string) (sherpa.Sherpa, error) {	
	log.Printf("Factory making a new %s\n", provider)
	var s sherpa.Sherpa
	switch provider {
	case "mssql":
		connection := cfg.DBConfig.Providers[provider]
		s = mssql.New(connection)
		break
	default:
		return nil, fmt.Errorf("No provider found for %s", provider)
	}
	return s, nil
}
