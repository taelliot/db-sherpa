package sherpa

type Sherpa interface {
	DropDB(name string) error
	AttachDB(path string) error
}
