package aws

import (
	"gopkg.in/yaml.v2"
	"log"

	config "bitbucket.org/taelliot/db-sherpa/src/config"
)

var environment_file = ""

type credentials struct {
	Profile   string `yaml:"profile"`
	Output    string `yaml:"output"`
}

var Credentials credentials

func init() {
	log.Println("AWS init...")
	Credentials.Parse(*config.RawConfig)
}

func (c *credentials) Parse(data []byte) error {
	err := yaml.Unmarshal(data, c)
	if err != nil {
		log.Fatal(err)
	}
	if c.Profile == "" {
		log.Fatalf("Profile not set in environment file.")
	}	
	if c.Output == "" {
		log.Fatalf("Profile not set in environment file.")
	}
	return nil
}
