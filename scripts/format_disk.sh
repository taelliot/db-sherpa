#! powershell?

diskpart

# list disk - find the disk name
select disk 1

# size is in MB
create partition primary size=10000

# format it..
format fs=ntfs quick
assign letter = S

